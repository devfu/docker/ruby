# Dev Fu! Maintained Ruby Docker Images

https://gitlab.com/devfu/docker/ruby/container_registry

| Branch     | Version | Base       | Status                            |
| ---------- | ------- | ---------- | --------------------------------- |
| 2.4-alpine | 2.4.5   | Alpine 3.9 | [![status][2.4-alpine]][pipeline] |
| 2.5-alpine | 2.5.3   | Alpine 3.9 | [![status][2.5-alpine]][pipeline] |
| 2.6-alpine | 2.6.1   | Alpine 3.9 | [![status][2.6-alpine]][pipeline] |

<!-- references -->

[pipeline]: https://gitlab.com/devfu/docker/ruby/pipelines
[2.4-alpine]: https://gitlab.com/devfu/docker/ruby/badges/2.4-alpine/pipeline.svg
[2.5-alpine]: https://gitlab.com/devfu/docker/ruby/badges/2.4-alpine/pipeline.svg
[2.6-alpine]: https://gitlab.com/devfu/docker/ruby/badges/2.4-alpine/pipeline.svg
